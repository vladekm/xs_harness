from HTMLParser import HTMLParser
import zope.interface
import duckduckgo

from duckduckgo_harness.sdk import IDuckDuckGoPort


class DuckDuckGoAPI(object):
    zope.interface.implements(IDuckDuckGoPort)

    def get_search_result(self, search_term):
        return duckduckgo.query(search_term)

    def parse_arithmetic_result(self, search_result):
        return int(strip_tags(search_result.answer.text))


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()
