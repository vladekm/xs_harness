from selenium import webdriver
import zope.interface

from duckduckgo_harness.sdk import IDuckDuckGoPort

from .pages import HomePage
from .pages import ResultsPage
from .page_components import SearchBox
from .page_components import AnswerWidget


class Placeholder(object):
    pass


class SeleniumAdapter(object):
    """WebDriver based adapter

    Provides functionality necessary to communicate with DuckDuckGo
    via Web Interface using Webdriver.
    """
    zope.interface.implements(IDuckDuckGoPort)

    URL = 'http://www.duckduckgo.com'

    def __init__(self):
        self.pages = Placeholder()
        profile = webdriver.FirefoxProfile()
        self.browser = webdriver.Firefox(firefox_profile=profile)
        self._initialize_pages(self.browser)

    def __del__(self):
        self.browser.close()

    def _initialize_pages(self, browser):
        self.pages.search_page = HomePage(
            self.browser, self.URL, search_box=SearchBox(self.browser)
        )
        self.pages.results_page = ResultsPage(
            self.browser, self.URL, answer_widget=AnswerWidget(self.browser)
        )

    def get_search_result(self, search_term):
        self.pages.search_page.search(search_term)
        return self.pages.results_page.read_the_result()

    def parse_arithmetic_result(self, search_result):
        return int(search_result)
