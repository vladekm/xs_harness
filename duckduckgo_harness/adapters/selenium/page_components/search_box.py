from collections import namedtuple

import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

Locator = namedtuple('Locator', 'type value')

DEFAULT_WAIT_TIME = 1000

class SearchBox(object):
    search_input = Locator(By.CLASS_NAME, 'search__input--adv')

    def __init__(self, browser):
        self.browser = browser

    def search(self, term):
        search_box = self.browser.find_element_by_class_name(
            self.search_input.value
        )
        WebDriverWait(self.browser, DEFAULT_WAIT_TIME).until(EC.visibility_of_element_located(
            tuple(self.search_input)
        ))
        search_box.send_keys(term)
        search_box.send_keys(Keys.RETURN)
