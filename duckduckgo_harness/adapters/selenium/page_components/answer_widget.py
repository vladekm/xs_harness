from collections import namedtuple

import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

DEFAULT_WAIT_TIME = 10

Locator = namedtuple('Locator', 'type value')


class AnswerWidget(object):
    calculator = Locator(By.CLASS_NAME, 'js-zci-link--calculator')
    result = Locator(By.ID, 'display')

    def __init__(self, browser):
        self.browser = browser

    def read_the_result(self):
        # wait for the calculator tab to be active and click it
        WebDriverWait(self.browser, DEFAULT_WAIT_TIME).until(
            EC.visibility_of_element_located(
                tuple(self.calculator)
            )
        )
        self.browser.find_element_by_class_name(
            self.calculator.value
        ).click()
        WebDriverWait(self.browser, DEFAULT_WAIT_TIME).until(
            EC.presence_of_element_located(
                tuple(self.result)
            )
        )
        return self.browser.find_element_by_id(
            self.result.value
        ).text
