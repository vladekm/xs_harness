from .selenium.selenium_adapter import SeleniumAdapter
from .api.rest_adapter import DuckDuckGoAPI


__all__ = ['SeleniumAdapter', 'DuckDuckGoAPI']
