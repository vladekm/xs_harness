import zope.interface
from zope.interface.verify import verifyClass


class IDuckDuckGoPort(zope.interface.Interface):

    """Define the shape of communication for the adapters.

    All the adapters must implement this interface.
    """

    def get_search_result(search_term):
        """Return search results"""

    def parse_arithmetic_result(search_result):
        """Extracts the arithmetic result from the search results"""


class DuckDuckGoSDK(object):

    def __init__(self, adapter):
        verifyClass(IDuckDuckGoPort, adapter.__class__)
        self.adapter = adapter

    def get_search_result(self, search_term):
        return self.adapter.get_search_result(search_term)

    def parse_arithmetic_result(self, search_result):
        return self.adapter.parse_arithmetic_result(search_result)
