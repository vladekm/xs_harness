from duck_duck_go import DuckDuckGoSDK, IDuckDuckGoPort


__all__ = ['DuckDuckGoSDK', 'IDuckDuckGoPort']
