from behave import given, when, then


@given(u'that I search DuckDuckGo with term: "{search_term}"')
def search_duckduckgo(context, search_term):
    context.data['raw_result'] = context.sdk.get_search_result(search_term)


@when(u'I parse the arithmetic result')
def fetch_result(context):
    context.data['result'] = context.sdk.parse_arithmetic_result(
        context.data['raw_result']
    )


@then(u'the result is "{expected_result}"')
def check_result(context, expected_result):
    assert int(expected_result) == context.data['result']
