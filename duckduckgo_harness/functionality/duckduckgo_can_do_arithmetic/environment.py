import os

from duckduckgo_harness.adapters import SeleniumAdapter
from duckduckgo_harness.adapters import DuckDuckGoAPI
from duckduckgo_harness.sdk import DuckDuckGoSDK


class Placeholder(object):
    pass


def before_all(context):
    adapter_to_use = os.environ.get('ADAPTER', 'api')
    if adapter_to_use == 'www':
        adapter = SeleniumAdapter()
    elif adapter_to_use == 'api':
        adapter = DuckDuckGoAPI()
    else:
        raise Exception('Unknown adapter')
    context.sdk = DuckDuckGoSDK(adapter)
    context.data = {}


def after_all(context):
    if getattr(context, 'browser', None):
        context.browser.close()
